<?php

namespace Drupal\view_profiles_perms;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\BundlePermissionHandlerTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the dynamic permissions for view_profile_perms module.
 */
class ViewProfilesPermsPermissions implements ContainerInjectionInterface {
  use BundlePermissionHandlerTrait;
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ViewProfilesPermsPermissions instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Get role permissions.
   *
   * @return array
   *   Permissions array.
   */
  public function permissions() {
    return $this->generatePermissions(Role::loadMultiple(), [$this, 'buildPermissions']);
  }

  /**
   * Builds a list of view profiles permissions for a given role.
   *
   * @param \Drupal\user\RoleInterface $role
   *   The role to build the permissions for.
   *
   * @return array
   *   An array of permission names and descriptions.
   */
  protected function buildPermissions(RoleInterface $role) {
    // Ignore the anonymous and authenticated roles.
    if ($role->id() === RoleInterface::ANONYMOUS_ID || $role->id() === RoleInterface::AUTHENTICATED_ID) {
      return [];
    }
    $role_name = $role->label();
    $id = $role->id();
    $args = ['%role_name' => $role_name];

    return ["access $id users profiles" => ['title' => $this->t('Access %role_name users profiles', $args)]];
  }

}
